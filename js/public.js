(function loadCxwc() {
  try {
    const {
      isAdminPage,
      config: { serviceAccount, cxwcSrc },
    } = drupalSettings.cinnox;

    if (!isAdminPage) {
      window.wcSettings = {
        serviceName: serviceAccount,
      };

      !(function (e, t, c) {
        var n,
          s = e.getElementsByTagName(t)[e.getElementsByTagName(t).length - 1];
        e.getElementById(c) ||
          ((n = e.createElement(t)),
          (n.id = c),
          (n.defer = !0),
          (n.src = cxwcSrc),
          s.parentNode.insertBefore(n, s));
      })(document, 'script', 'wc-dom-id');
    }
  } catch (wcLoadingError) {
    // Just don't crash the JS thread, no actual handling is available
    console.error(wcLoadingError);
  }
})();
