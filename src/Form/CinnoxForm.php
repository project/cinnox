<?php

namespace Drupal\cinnox\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

define( 'M800_CX_SERVICE_REGEX', '/^[a-zA-Z0-9][a-zA-Z0-9-]*[a-zA-Z0-9](\.[a-zA-Z0-9][a-zA-Z0-9-]*[a-zA-Z0-9])*\.(cinnox|m800)\.(com|cn)$/' );

/**
 * This form serve as the most basic use case of configuration.
 * If we do not need extensive integration/UX for the configuration page,
 * this form can be used and we are good.
 * Otherwise custom page and submission handlers can be created in the controller.
 * @see https://www.drupal.org/docs/creating-custom-modules/create-a-custom-page
 *
 * Simply specify a `_form` property in the routing file, then we can use this.
 * @example
 * cinnox.settings:
 *   path: '/admin/config/cinnox/settings'
 *   defaults:
 *     _form: '\Drupal\cinnox\Form\cinnoxForm'
 *     _title: 'CINNOX settings'
 *   requirements:
 *     _permission: 'administer site configuration'
 */
class CinnoxForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cinnox_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    // Default/current settings
    $config = $this->config('cinnox.settings');

    $form['subscribe_text'] = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => $this->t('If you didn\'t subscribe CINNOX service, please <a href="https://www.cinnox.com/essentials-sign-up" alt="Sign up CINNOX" target="_blank">click here to subscribe CINNOX for free</a>!'),
    ];

    // Service account field
    $form['service_account'] = [
      '#type' => 'textfield',
      '#required' => true,
      '#title' => $this->t('Service Account:'),
      '#default_value' => $config->get('service_account'),
      '#description' => $this->t('Service Account is an unique string (e.g. <span style="text-decoration: underline">example.cinnox.com</span>) that represents your service in CINNOX. It is also the url that you login to your dashboard. You can locate it in the Installation section of the Dashboard.'),
    ];

    // Add an additional button which launches the CINNOX dashboard
    $form['actions']['launch_dashboard'] = [
      '#type' => 'button',
      '#value' => $this->t('Launch CINNOX Dashboard'),
      '#attributes' => [
        'onclick' => 'return false;',
        'disabled' => 'disabled',
        'data-service-account' => $config->get('service_account'),
      ],
      '#attached' => [
        'library' => [
          'cinnox/admin'
        ]
      ]
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $message = parent::validateForm($form, $form_state);

    $service_account = $form_state->getValue('service_account');
    if (!$service_account || empty($service_account)) {
      $form_state->setErrorByName('service_account', $this->t('Service Account is mandatory'));
    } else if (!preg_match(M800_CX_SERVICE_REGEX, $service_account)) {
      $form_state->setErrorByName('service_account', $this->t('Service Account is not valid, it must follow <span style="text-decoration: underline">example.cinnox.com</span> format'));
    }

    return $message;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('cinnox.settings');
    $config->set('service_account', $form_state->getValue('service_account'));
    $config->save();

    return parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'cinnox.settings',
    ];
  }

}
